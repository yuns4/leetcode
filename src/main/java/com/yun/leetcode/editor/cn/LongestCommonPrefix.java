//编写一个函数来查找字符串数组中的最长公共前缀。 
//
// 如果不存在公共前缀，返回空字符串 ""。 
//
// 示例 1: 
//
// 输入: ["flower","flow","flight"]
//输出: "fl"
// 
//
// 示例 2: 
//
// 输入: ["dog","racecar","car"]
//输出: ""
//解释: 输入不存在公共前缀。
// 
//
// 说明: 
//
// 所有输入只包含小写字母 a-z 。 
// Related Topics 字符串

package com.yun.leetcode.editor.cn;

// 【14】 最长公共前缀
public class LongestCommonPrefix {
    public static void main(String[] args) {
        Solution solution = new LongestCommonPrefix().new Solution();
        String[] ary = {"aa", "a"};
        System.out.println(solution.longestCommonPrefix(ary));
    }


    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public String longestCommonPrefix(String[] strs) {
            if (strs.length == 0) {
                return "";
            }
            char[] chs = strs[0].toCharArray();
            int point = chs.length;
            for (int i = 1; i < strs.length; i++) {
                char[] chars = strs[i].toCharArray();
                for (int j = 0; j < chars.length && j < point; j++) {
                    if (chars[j] != chs[j]) {
                        point = j;
                        break;
                    }
                }
                if (point > chars.length) {
                    point = chars.length;
                }
            }
            return new String(chs, 0, point);
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

//    class Solution {
//        public String longestCommonPrefix(String[] strs) {
//            StringBuilder sb = new StringBuilder();
//            for (int i = 0; i < strs[0].length(); i++) {
//                char c = strs[0].charAt(i);
//                for (int j = 0; j < strs.length; j++) {
//                    if (strs[j].charAt(i) != c) {
//                        return sb.toString();
//                    }
//                }
//                sb.append(c);
//            }
//            return sb.toString();
//        }
//    }
}