//给定一个链表，删除链表的倒数第 n 个节点，并且返回链表的头结点。 
//
// 示例： 
//
// 给定一个链表: 1->2->3->4->5, 和 n = 2.
//
//当删除了倒数第二个节点后，链表变为 1->2->3->5.
// 
//
// 说明： 
//
// 给定的 n 保证是有效的。 
//
// 进阶： 
//
// 你能尝试使用一趟扫描实现吗？ 
// Related Topics 链表 双指针

package com.yun.leetcode.editor.cn;

// 【19】 删除链表的倒数第N个节点
public class RemoveNthNodeFromEndOfList {
    public static void main(String[] args) {
        Solution solution = new RemoveNthNodeFromEndOfList().new Solution();
        ListNode listNode = ListNode.createListNode(new int[]{1, 2,3,4,5});
        ListNode listNode1 = solution.removeNthFromEnd(listNode, 2);
        System.out.println(listNode1);
    }


//leetcode submit region begin(Prohibit modification and deletion)

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    class Solution {
        public ListNode removeNthFromEnd(ListNode head, int n) {
            ListNode left = head;
            ListNode right = head;
            while (n-- > 0) {
                if (right != null) {
                    right = right.next;
                }
            }
            if (right == null) {
                head = head.next;
                return head;
            }
            while (right.next != null) {
                right = right.next;
                left = left.next;
            }
            left.next = left.next.next;
            return head;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}