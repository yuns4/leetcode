//给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。 
//
// 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。 
//
// 
//
// 示例: 
//
// 输入："23"
//输出：["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
// 
//
// 说明: 
//尽管上面的答案是按字典序排列的，但是你可以任意选择答案输出的顺序。 
// Related Topics 字符串 回溯算法

package com.yun.leetcode.editor.cn;

import java.util.ArrayList;
import java.util.List;

// 【17】 电话号码的字母组合
public class LetterCombinationsOfAPhoneNumber {
    public static void main(String[] args) {
        Solution solution = new LetterCombinationsOfAPhoneNumber().new Solution();
        System.out.println(solution.letterCombinations(""));
    }


    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {

        private String[] dig = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

        public List<String> letterCombinations(String digits) {
            List<String> result = new ArrayList<>();
            if ("".equals(digits)) {
                return result;
            }
            StringBuilder sb = new StringBuilder();
            combine(sb, digits, 0, result);

            return result;
        }

        private void combine(StringBuilder sb, String digits, int index, List<String> result) {
            if (sb.length() >= digits.length()) {
                result.add(sb.toString());
                return;
            }
            for (int i = 0; i < dig[digits.charAt(index) - '0'].length(); i++) {
                sb.append(dig[digits.charAt(index) - '0'].charAt(i));
                combine(sb, digits, index + 1, result);
                if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
            }

        }
    }
//leetcode submit region end(Prohibit modification and deletion)
// 回溯法
//
}