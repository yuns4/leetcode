package com.yun.leetcode.editor.cn;

/**
 * Created by lsy
 * on 2019/10/28 10:07.
 */
public class ListNode {
    int val;
    ListNode next;

    private ListNode(){}
    public ListNode(String a){}
    public ListNode(int x) {
        val = x;
    }

    @Override
    public String toString() {
        String s = val + " -> ";
        boolean subNull = (next == null);
        while (next != null) {
            s += next.toString();
            next = next.next;
        }
        if (subNull) {
            s = s.substring(0, s.length() - 4);
        }
        return s;
    }
    public static ListNode createListNode(int[] ary){
        ListNode head = new ListNode(-1);
        ListNode tmp = head;
        for (int i : ary) {
            tmp.next = new ListNode(i);
            tmp = tmp.next;
        }
        return head.next;
    }


}
