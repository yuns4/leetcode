//给定两个大小为 m 和 n 的有序数组 nums1 和 nums2。 
//
// 请你找出这两个有序数组的中位数，并且要求算法的时间复杂度为 O(log(m + n))。 
//
// 你可以假设 nums1 和 nums2 不会同时为空。 
//
// 示例 1: 
//
// nums1 = [1, 3]
//nums2 = [2]
//
//则中位数是 2.0
// 
//
// 示例 2: 
//
// nums1 = [1, 2]
//nums2 = [3, 4]
//
//则中位数是 (2 + 3)/2 = 2.5
// 
// Related Topics 数组 二分查找 分治算法

package com.yun.leetcode.editor.cn;

// 【4】 寻找两个有序数组的中位数
public class MedianOfTwoSortedArrays {
    public static void main(String[] args) {
        Solution solution = new MedianOfTwoSortedArrays().new Solution();
        int[] a1 = {1, 3};
        int[] b1 = {2};
        System.out.println(solution.findMedianSortedArrays(a1, b1));
    }


    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public double findMedianSortedArrays(int[] nums1, int[] nums2) {
            int m = nums1.length;
            int n = nums2.length;
            int left = (m + n + 1) / 2;
            int right = (m + n + 2) / 2;
            return (findKth(nums1, 0, nums2, 0, left) + findKth(nums1, 0, nums2, 0, right)) / 2;
        }

        public double findKth(int[] nums1, int x, int[] nums2, int y, int k) {
            if (nums1.length <= x) {
                return nums2[y+k-1];
            }
            if (nums2.length <= y) {
                return nums1[x + k - 1];
            }
            // 找第一个数字，就是两个里最小的一个，走到这一步两个数组各只有一个数字
            if (k == 1) {
                return Math.min(nums1[x], nums2[y]);
            }
            int mid1 = (x + k / 2 - 1 < nums1.length) ? nums1[x + k / 2 - 1] : Integer.MAX_VALUE;
            int mid2 = (y + k / 2 - 1 < nums2.length) ? nums2[y + k / 2 - 1] : Integer.MAX_VALUE;
            if (mid1 < mid2) {
                return findKth(nums1, x + k / 2, nums2, y, k - k / 2);
            } else {
                return findKth(nums1, x, nums2, y + k / 2, k - k / 2);
            }
        }
    }
//leetcode submit region end(Prohibit modification and deletion)
// xxxx一定要利用两个表都是有序的
    // 1. xxx两个表刚好能连上a0 >= bn || b0 >= an, mid
    // 2.xxx

    // 主要利用找第nth的数字，比较两个数组最中间的数，谁小谁就往右边缩范围，相应的找的nth这个排序也得减少，
    // 因为往右边缩范围，等于截取了一部分左侧的数据

    // 这道题让我们求两个有序数组的中位数，而且限制了时间复杂度为O(log (m+n))，看到这个时间复杂度，自然而然的想到了应该使用二分查找法来求解。
    // 那么回顾一下中位数的定义，如果某个有序数组长度是奇数，那么其中位数就是最中间那个，如果是偶数，那么就是最中间两个数字的平均值。
    // 这里对于两个有序数组也是一样的，假设两个有序数组的长度分别为m和n，由于两个数组长度之和 m+n 的奇偶不确定，因此需要分情况来讨论，
    // 对于奇数的情况，直接找到最中间的数即可，偶数的话需要求最中间两个数的平均值。为了简化代码，不分情况讨论，我们使用一个小trick，我
    // 们分别找第 (m+n+1) / 2 个，和 (m+n+2) / 2 个，然后求其平均值即可，这对奇偶数均适用。加入 m+n 为奇数的话，那么其实 (m+n+1) / 2
    // 和 (m+n+2) / 2 的值相等，相当于两个相同的数字相加再除以2，还是其本身。
    //
    //这里我们需要定义一个函数来在两个有序数组中找到第K个元素，下面重点来看如何实现找到第K个元素。
    // 首先，为了避免产生新的数组从而增加时间复杂度，我们使用两个变量i和j分别来标记数组nums1和nums2的起始位置。然后来处理一些边界问题
    // ，比如当某一个数组的起始位置大于等于其数组长度时，说明其所有数字均已经被淘汰了，相当于一个空数组了，那么实际上就变
    // 成了在另一个数组中找数字，直接就可以找出来了。还有就是如果K=1的话，那么我们只要比较nums1和nums2的起始位置i和j上的数字就可
    // 以了。难点就在于一般的情况怎么处理？因为我们需要在两个有序数组中找到第K个元素，为了加快搜索的速度，我们要使用二分法，对K二分，意
    // 思是我们需要分别在nums1和nums2中查找第K/2个元素，注意这里由于两个数组的长度不定，所以有可能某个数组没有第K/2个数字，所以我们需要先
    // 检查一下，数组中到底存不存在第K/2个数字，如果存在就取出来，否则就赋值上一个整型最大值。如果某个数组没有第K/2个数字，那么我们就淘汰另一
    // 个数字的前K/2个数字即可。有没有可能两个数组都不存在第K/2个数字呢，这道题里是不可能的，因为我们的K不是任意给的，而是给的m+n的中间
    // 值，所以必定至少会有一个数组是存在第K/2个数字的。最后就是二分法的核心啦，比较这两个数组的第K/2小的数字midVal1和midVal2的大小，如
    // 果第一个数组的第K/2个数字小的话，那么说明我们要找的数字肯定不在nums1中的前K/2个数字，所以我们可以将其淘汰，将nums1的起始位置向后
    // 移动K/2个，并且此时的K也自减去K/2，调用递归。反之，我们淘汰nums2中的前K/2个数字，并将nums2的起始位置向后移动K/2个，并且此时的K


}