//给出两个 非空 的链表用来表示两个非负的整数。其中，它们各自的位数是按照 逆序 的方式存储的，并且它们的每个节点只能存储 一位 数字。
//
// 如果，我们将这两个数相加起来，则会返回一个新的链表来表示它们的和。 
//
// 您可以假设除了数字 0 之外，这两个数都不会以 0 开头。 
//
// 示例： 
//
// 输入：(2 -> 4 -> 3) + (5 -> 6 -> 4)
//输出：7 -> 0 -> 8
//原因：342 + 465 = 807
// 
// Related Topics 链表 数学

package com.yun.leetcode.editor.cn;

public class AddTwoNumbers {
    public static void main(String[] args) {
        ListNode n1 = ListNode.createListNode(new int[]{7,9,0});
        ListNode b1 = ListNode.createListNode(new int[]{5});


        Solution solution = new AddTwoNumbers().new Solution();
        ListNode listNode = solution.addTwoNumbers(n1, b1);
        System.out.println(listNode);
        System.out.println(n1);
        System.out.println(b1);
    }


//leetcode submit region begin(Prohibit modification and deletion)

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    class Solution {
        public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
            ListNode rs = new ListNode(0);
            ListNode tmp = rs;
            int bit = 0;
            while (l1 != null && l2 != null) {
                int i = l1.val + l2.val + bit;
                tmp.next = new ListNode(i % 10);
                bit = i / 10;
                l1 = l1.next;
                l2 = l2.next;
                tmp = tmp.next;
            }
            while (l1 != null) {
                int x = l1.val + bit;
                tmp.next = new ListNode(x % 10);
                bit = x / 10;
                l1 = l1.next;
                tmp = tmp.next;
            }
            while (l2 != null) {
                int x = l2.val + bit;
                tmp.next = new ListNode(x % 10);
                bit = x / 10;
                l2 = l2.next;
                tmp = tmp.next;
            }
            if (bit != 0) {
                tmp.next = new ListNode(bit);
            }

            return rs.next;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}