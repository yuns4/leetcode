package com.yun.leetcode;

import com.yun.leetcode.editor.cn.ListNode;
import sun.misc.Unsafe;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by lsy
 * on 2019/11/6 11:36.
 */
public class UnSafeTest {
    public static void main(String[] args) throws Exception {
        Class<?> unsafeClass = Class.forName("sun.misc.Unsafe");
        Field f = unsafeClass.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        Unsafe unsafe = (Unsafe) f.get(null);
        Method allocateInstance = unsafeClass.getDeclaredMethod("allocateInstance", Class.class);

        int[] ary = {22,33,55,77,88};
        int i = unsafe.arrayBaseOffset(int[].class);
        System.out.println(i);
        System.out.println(unsafe.arrayIndexScale(int[].class));
        System.out.println(unsafe.arrayIndexScale(long[].class));
        System.out.println(unsafe.arrayIndexScale(double[].class));
        System.out.println(unsafe.arrayIndexScale(float[].class));

        System.out.println(unsafe.addressSize());
        System.out.println(unsafe.pageSize());
    }
}
