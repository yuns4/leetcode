package com.yun.leetcode;

import java.math.BigDecimal;

/**
 * Created by lsy
 * on 2019/10/11 10:04.
 */
public class Test {
    public static void main(String[] args) throws InterruptedException {
        int a = 310-64-238-7;
        BigDecimal b1 = new BigDecimal(a);
        System.out.println(b1.compareTo(BigDecimal.ONE));
    }
}
