package com.yun.jvm;

/**
 * Created by lsy
 * on 2019/11/1 11:10.
 * VM Args: -Xss2M
 */
public class TestStackOOM {

    private void dontStop() {
        while (true) {
        }
    }

    public void stackLeakByThread() {
        while (true) {
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    dontStop();
                }
            });
            thread.start();
        }
    }

    public static void main(String[] args) throws Throwable {
        TestStackOOM oom = new TestStackOOM();
        // oom.stackLeakByThread();
    }

    //Exception in thread"main"java.lang.OutOfMemoryError：unable
    // to create new native thread
}
