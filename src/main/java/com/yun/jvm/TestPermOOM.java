package com.yun.jvm;

import java.util.ArrayList;

/**
 * Created by lsy
 * on 2019/11/1 11:35.
 * VM Args: -XX:PermSize=1M -XX:MaxPermSize=1M
 * 因为1.9已经去掉了永久代，所以这个例子在jdk1.8中不会出异常
 * Java HotSpot(TM) 64-Bit Server VM warning: ignoring option PermSize=1k; support was removed in 8.0
 * Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=1k; support was removed in 8.0
 */
public class TestPermOOM {
    public static void main(String[] args) {
//        ArrayList<String> list = new ArrayList<>();
//        int i = 0;
//        while (true) {
//            list.add(String.valueOf(i++).intern());
//            System.out.println(list.size());
//        }


        String str1 = new StringBuilder("jav").append("a").toString();
        System.out.println(str1.intern() == str1); // false

        String str2 = new StringBuilder("计算机").append("soft").toString();
        System.out.println(str2.intern() == str2); // true
        // java 字符串之前出现过了，str1是sb，是堆上的地址，intern之后返回的是常量池中已经有的引用
        // 计算机软件字符串之前没有出现过，在堆上先创建了，intern然后常量池现在记录首次出现的这个实例的引用，然后返回

    }
}
