package com.yun.jvm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsy
 * on 2019/11/1 10:58.
 * 测试堆溢出
 * VM Args: -Xms20m -Xmx20m -XX:+HeapDumpOnOutOfMemoryError
 */
public class TestOOM {
    static class OOMObject{

    }

    public static void main(String[] args) {
        List<OOMObject> list = new ArrayList<>();
        while (true) {
            list.add(new OOMObject());
        }
    }
    //C:\JAVA\JDK8\bin\java -Xms20m -Xmx20m -XX:+HeapDumpOnOutOfMemoryError
    //java.lang.OutOfMemoryError: Java heap space
    //Dumping heap to java_pid34024.hprof ...
    //Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
    //	at java.util.Arrays.copyOf(Arrays.java:3210)
    //	at java.util.Arrays.copyOf(Arrays.java:3181)
    //	at java.util.ArrayList.grow(ArrayList.java:261)
    //	at java.util.ArrayList.ensureExplicitCapacity(ArrayList.java:235)
    //	at java.util.ArrayList.ensureCapacityInternal(ArrayList.java:227)
    //Heap dump file created [28171037 bytes in 0.172 secs]
}
