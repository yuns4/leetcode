package com.yun.jvm;

/**
 * Created by lsy
 * on 2019/11/1 11:04.
 * VM Args : -Xss128k
 */
public class TestStackSOF {
    private int stackLength = 1;

    public void stackLeak() {
        stackLength++;
        stackLeak();
    }

    public static void main(String[] args) throws Throwable {
        TestStackSOF oom = new TestStackSOF();
        try {
            oom.stackLeak();
        } catch (Throwable e) {
            System.out.println("stack length：" + oom.stackLength);
            throw e;
        }
    }
    //stack length：1579
    //Exception in thread "main" java.lang.StackOverflowError
    //	at com.yun.jvm.TestStackSOF.stackLeak(TestStackSOF.java:13)
    //	at com.yun.jvm.TestStackSOF.stackLeak(TestStackSOF.java:13)
}
