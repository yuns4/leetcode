package com.yun.redis.practice;

import org.junit.Test;
import redis.clients.jedis.DebugParams;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.zip.CRC32;

/**
 * Created by lsy
 * on 2019/12/4 11:42.
 */
public class RedisTest {
    public static void main(String[] args) {
        Jedis conn = RedisUtils.getConnection();
        System.out.println(conn.hget("user:11", "name"));
    }

    @Test
    public void testDebugObject() {
        Jedis conn = RedisUtils.getConnection();
        System.out.println(conn.rpush("d_lista", "aa", "aa", "aa", "aa"));
        String dList = conn.debug(DebugParams.OBJECT("d_lista"));
        String[] split = dList.split(" ");
        for (String s : split) {
            System.out.println(s);
        }
    }

    @Test
    public void testCRC32() {
        CRC32 crc = new CRC32();
        crc.update("ds1".getBytes());
        System.out.println(crc.getValue());
        System.out.println(Integer.MAX_VALUE);
        System.out.println(UUID.randomUUID().toString().replace("-", ""));
    }

    @Test
    public void testLuaEval() {
        Jedis conn = RedisUtils.getConnection();
        System.out.println(conn.eval("return true"));
    }

    @Test
    public void testLuaScript() {
        // eval直接执行，eval先设置好脚本，后面用返回的sha调用，并且传参
        // 获得键名sn的字符串
        Jedis conn = RedisUtils.getConnection();
        String shaKey = conn.scriptLoad("return redis.call('get', KEYS[1])");
        ArrayList<String> keys = new ArrayList<>();
        keys.add("sn");
        ArrayList<String> args = new ArrayList<>();
        String result = (String) conn.evalsha(shaKey, keys, args);
        System.out.println(result);
    }

    @Test
    public void testLuaCreateUser() {
        String userName = "zs";

        // lua 参数下标从1开始
        // lua 不等于使用"~="

//        while num < 5 do
//            print(num);
//        num = num + 1
//        end


//        if a and b then --逻辑与
//        print("大于9")
//        elseif a or b5 then --逻辑或
//        print("大于5")
//        elseif not a then -- 逻辑否
//        print("其它数")

//        for i = 1,5 do --默认递增+1
//        print(i)
//        end
//
//        for i = 5,1,-1 do --递减1
//        print(i)
//        end
        String luaScript =
                "local login = redis.call('hget', KEYS[1], 'name') " +//获得锁
                        "if login then " +
                        "   return false " +
                        "end " +
                        "local id = redis.call('incr', KEYS[2]) " +
                        "redis.call('hmset',KEYS[1]," +
                        "   'name',KEYS[3], " +
                        "   'id',id) " +
                        "return id";
        Jedis conn = RedisUtils.getConnection();
        String shaKey = conn.scriptLoad(luaScript);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("user:" + userName);
        keys.add("user:id:");
        keys.add(userName);
        ArrayList<String> args = new ArrayList<>();
        System.out.println(shaKey);
        Object evalsha = conn.evalsha(shaKey, keys, args);
        System.out.println(evalsha);
    }

    @Test
    public void testAcquireLockByLua() {
        String name = "user:" + "zs";
        String timeout = "5022";

        Jedis conn = RedisUtils.getConnection();
        String script = "if redis.call('exists', KEYS[1]) == 0 then " +
                "   return redis.call('setex',KEYS[1],unpack(ARGV))" +
                " end" +
                " return 'false'";
        String token = UUID.randomUUID().toString();
        List<String> keys = new ArrayList<>();
        keys.add("lock:" + name);
        List<String> args = new ArrayList<>();
        args.add(timeout);
        args.add(token);
        if ("OK".equals(conn.eval(script, keys, args))) {
            // 获取成功
        }
//         获取失败
//        System.out.println(conn.eval(script, keys, args));
    }

    @Test
    public void testReleaseLockByLua() {
        String name = "user:" + "zs";
        String token = "33";

        Jedis conn = RedisUtils.getConnection();
        String script = "if redis.call('get',KEYS[1]) == ARGV[1] then" +
                "   return redis.call('del',KEYS[1]) or true " +
                "end";
        List<String> keys = new ArrayList<>();
        keys.add("lock:" + name);
        List<String> args = new ArrayList<>();
        args.add(token);
        Object result = conn.eval(script, keys, args);
        if(result != null && (Long)result == 1){
            // 解锁成功
        }
//        System.out.println(conn.eval(script, keys, args));
    }

}
