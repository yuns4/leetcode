package com.yun.redis.practice;

import redis.clients.jedis.Jedis;

/**
 * Created by lsy
 * on 2019/12/11 15:02.
 */
public class RedisUtils {

    private static final String host = "39.98.74.32";

    public static Jedis getConnection() {

        //创建jedis对象
        Jedis jedis = new Jedis(host, 6999);
        jedis.auth("zx000000");
        return jedis;
    }

}
