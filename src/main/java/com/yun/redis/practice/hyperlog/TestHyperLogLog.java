package com.yun.redis.practice.hyperlog;

import com.yun.redis.practice.RedisUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

/**
 * Created by lsy
 * on 2019/12/25 10:06.
 */
public class TestHyperLogLog {
    public static void main(String[] args) {
        Jedis con = RedisUtils.getConnection();
        con.del("holder");
        Pipeline pipelined = con.pipelined();
        for (int i = 1; i < 100000; i++) {

            pipelined.pfadd("holder", i + "");
        }
        pipelined.sync();
        long holderCount = con.pfcount("holder");
        System.out.println("holderCount = " + holderCount + " i= " + 100000);
        // 10000  holderCount = 9986 i= 10000
    }
}
