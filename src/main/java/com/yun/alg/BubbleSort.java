package com.yun.alg;

import java.util.Arrays;

/**
 * Created by lsy
 * on 2020/6/11 15:42.
 */
public class BubbleSort {

    public static void main(String[] args) {
        BubbleSort bubbleSort = new BubbleSort();
        int[] ary = {5, 4, 7, 88, 99, 33, 11, 33};
        bubbleSort.bubbleSort(ary);
        System.out.println(Arrays.toString(ary));
    }

    public void bubbleSort(int[] ary) {
        for (int i = 0; i < ary.length - 1; i++) {
            for (int j = 0; j < ary.length - i - 1; j++) {
                if(ary[j] > ary[j+1]){
                    swap(ary, j, j+1);
                }
            }
        }
    }

    public void swap(int[] ary, int a, int b)
    {
        int tmp = ary[a];
        ary[a] = ary[b];
        ary[b] = tmp;
    }
}
