### redis命令操作
> 键名区分大小写
#### 字符串(string)
命令| 描述
---| ------
GET|
SET|
INCR|自增
DECR|自减
INCRBY|自增固定值
DECRBY|自减固定值
INCRBYFLOAT|自增小数值
APPEND|追加字符
GETRANGE|获得start到end位置的字符，包含start和end
SETRANGE|从start开始设置
GETBIT|GETBIT key-name offset value
BITCOUNT|BITCOUNT key-name [start - end ] 统计二进制位里值为1的数量
BITOP|BITOP operation dest-key key1 key2.. （and，or，xor，not）执行并，或，异或，非操作存储到dest-key
#### 列表(list)
命令| 描述
---| ------
LPUSH/RPUSH|
LPOP|RPOP
LRANGE|
LINDEX|LINDEX key-name i 获取i位置的元素
LTRIM|LTRIM key-name start end 裁剪只保留start到end的元素
BLPOP/BRPOP|BLPOP/BRPOP key-name [key-name] timeout 从第一个非空列表中弹出一个元素，阻塞timeout秒内，谁先非空返回谁
RPOPLPUSH|RPOPLPUSH source-key dest-key 从source右侧弹出，入到dest左侧
BRPOPLPUSH|BRPOPLPUSH source-key dest-key timout 从source右侧弹出，入到dest左侧,阻塞timeout秒
#### 集合(set)
命令| 描述
---| ------
SADD|
SREM|
SISMEMBER|
SCARD|返回数量
SMEMBERS|返回所有集合里的东西
SRANDMEMBER|SRANDMEMBER key-name [count] 随机返回一个或多个，负数时会出现重复元素
SPOP|随机移除一个元素
SMOVE|SMOVE source-key dest-key itemr如果source里包含这个元素，就移除放入dest里
SDIFF/SDIFFSTORE|SDIFF key1 key2.../SDIFFSOTE dest key1 key2...找出存在于第一个集合不存在于其他集合里的元素，store存储到拎一个元素
SINTER/SINTERSTORE|同时存在于所有集合的元素，交集
SUNION/SUNIONSTORE|至少存在于一个集合的元素，并集

#### 散列(hash)
命令| 描述
---| ------
HMGET/HGET|多个操作和单个操作
HMSET/HSET|HMGET key-name key1 ..
HDEL|
HLEN|
HKEYS/HVALS|
HEXISTS|验证散列里key是否存在
HGETALL|获得所有
HINCRBY|
HINCRBYFLOAT|HINCRBYFLOAT key-name key 0.3f 选择散列的键增加float数
#### 有序集合(zset)
命令| 描述
---| ------
ZADD|ZADD key-name score member
ZREM|ZREM member
ZCARD|数量
ZINCRBY|ZINCRBY key-name increment member 给成员加上分支
ZCOUNT|ZCOUNT zs min max 返回在min和max之间分值的信息，必须有min和max
ZRANK|ZRANK key-name member返回member在集合中分值的排名
ZSCORE|ZSCORE key-name member返回member的分值
ZRANGE|ZRANGE key-name start end [WITHSCORES]返回分值从小到大排名在start和end的元素，WITHSCORES会一起返回分值
ZREVRANK|返回从大到小的排序，跟ZRANK
ZREVRANGE|从大到小返回分值从start到end
ZRANGEBYSCORE|ZRANGEBYSCORE key-name min max返回分值在min和max之间的member
ZREVRANGEBYSCORE|ZREVRANGEBYSCORE key-name min max返回分值在min和max之间的member,并从大到小
ZREMRANGEBYRANK|ZREMRANGEBYRANK key-name start end 移除排名在start到end间的member，start和end也移除
ZREMRANGEBYSCORE|移除分值在start和end之间的member
ZINTERSTORE|ZINTERSTORE dest key-count key1... [WEIGHTS weight [weight ...]] [AGGREGATE SUM/MIN/MAX]取交集，key-count是后面的key1...有几个的意思,结果的分值默认通过加法得出，可以指定
ZUNIONSTORE|ZUNIONSTORE dest key-count key1...
















